<!-- Menú móvil -->

<body>



<div id="mySidenav" class="sidenav">
<!-- Imagen Revimex en móvil -->



    <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>

    <div class="container">

    <div class="row">
        
            <div class="panel-group" id="accordion">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne"><h5 style="font-size: 13px;">CONÓCENOS</h5></a>
                        </h4>
                    </div>
                    <div id="collapseOne" class="panel-collapse collapse in" style="background-color: #291616">
                        <div class="panel-body" style="height: 133px;">
                            <table class="table">
                                <tr>
                                    <td>
                                        <a href="http://revimex.mx/sitio/mision.php"><h5 style="font-size: 14px;">Misión</h5></a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <a href="http://revimex.mx/sitio/requisitos.php"><h5 style="font-size: 14px;">¿Qué necesitas?</h5></a>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a href="http://revimex.mx/sitio/mapas2.html"><h5 style="font-size: 13px;">PROPIEDADES</h5></a>
                        </h4>
                    </div>
                    
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a href="http://www.revimex.mx/inversionistas.php"><h5 style="font-size: 13px;">INVERSIONISTAS</h5></a>
                        </h4>
                    </div>
                    
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a href="http://www.revimex.mx/sitio/recomendadores.php"><h5 style="font-size: 13px;">RECOMENDADORES</h5></a>
                        </h4>
                    </div>
                    
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a href="http://revimex.mx/casos.php"><h5 style="font-size: 13px;">CASOS DE ÉXITO</h5></a>
                        </h4>
                    </div>
                    
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a href="http://revimex.mx/sitio/vacantes.php"><h5 style="font-size: 13px;">VACANTES</h5></a>
                        </h4>
                    </div>
                    
                </div>

            </div>

    </div>
</div>


</div>


<!-- Menú escritorio -->
<!--Medidas para la imagen móvil-->
<style scoped>

    /* Apareciendo Imagen móvil 1*/

    @media screen and (min-width: 768px){
        div#imagencita{
            display: none;
        }
    }

    /* Desapareciendo Imagen móvil 1*/

    @media screen and (max-width: 629px){
        div#imagencita{
            display: none;
        }
    }

    /* Apareciendo Imagen móvil 2*/

    @media screen and (min-width: 630px){
        div#imagencitaDos{
            display: none;
        }
    }

    /* Desapareciendo Imagen móvil 2*/

    @media screen and (max-width: 499px){
        div#imagencitaDos{
            display: none;
        }
    }

    /* Apareciendo Imagen móvil 3*/

    @media screen and (min-width: 500px){
        div#imagencitaTres{
            display: none;
        }
    }

    /* Desapareciendo Imagen móvil 2*/

    @media screen and (max-width: 0px){
        div#imagencitaTres{
            display: none;
        }
    }

</style>
<nav class="navbar-inverse navbar-fixed-top">



        <div onclick="openNav()" class="navbar-header">
        <!-- Imagen móvil-->
        <div id="imagencita"><img src="images/WEB-JUN2017-LOGO-REVIMEX.jpg" style="width: 19%; height: 51px; position: absolute; margin-left: 40%; margin-right: 65%"></div>

        <div id="imagencitaDos"><img src="images/WEB-JUN2017-LOGO-REVIMEX.jpg" style="width: 26%; height: 51px; position: absolute; margin-left: 35%; margin-right: 66%"></div>

        <div id="imagencitaTres"><img src="images/WEB-JUN2017-LOGO-REVIMEX.jpg" style="width: 32%; height: 51px; position: absolute; margin-left: 33%; margin-right: 35%"></div>

            <button onclick="openNav()" type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                    data-target="#navbar"
                    aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>

        </div>

<div class="topnavMenu">
<div id="menu1">
  
  <a class="opcionMenu" href="#home">
    
      <p class="tamanoLetra"><div class="dropdown tamanoLetra">CONÓCENOS <img style="width: 13%;margin-top: -4%;margin-left: 2%;" src="images/flechita.png"><div class="dropdown-content">
        <p class="tamanoLetra"><button style="background-color: #0d0000; border: none;" onclick="window.location.href='http://revimex.mx/sitio/mision.php'">Misión</button></p>
        <p class="tamanoLetra"><button style="background-color: #0d0000; border: none;" onclick="window.location.href='http://revimex.mx/sitio/requisitos.php'">¿Qué necesitas?</button></p>
        </div>
        </div>
      </p>

</a>
      
  <a class="opcionMenu" href="http://revimex.mx/sitio/mapas2.html">
    
      <p class="tamanoLetra"><div class="dropdown tamanoLetra">PROPIEDADES<div class="dropdown-content">
        </div>
        </div>
      </p>

</a>
  <a class="opcionMenu" href="http://www.revimex.mx/inversionistas.php">
    
      <p class="tamanoLetra"><div class="dropdown tamanoLetra">INVERSIONISTAS<div class="dropdown-content">
        </div>
        </div>
      </p>

</a>
  <li><a href="http://revimex.mx/"><img src="images/WEB-JUN2017-LOGO-REVIMEX.jpg" class="logoindex"></a></li>


<a class="opcionMenu" href="http://www.revimex.mx/sitio/recomendadores.php">
    
      <p class="tamanoLetra"><div class="dropdown tamanoLetra">RECOMENDADORES<div class="dropdown-content">
        </div>
        </div>
      </p>

</a>
  <a class="opcionMenu" href="http://revimex.mx/casos.php">
    
      <p class="tamanoLetra"><div class="dropdown tamanoLetra">CASOS DE ÉXITO<div class="dropdown-content">
        </div>
        </div>
      </p>

</a>
  <a class="opcionMenu" href="http://revimex.mx/sitio/vacantes.php">
    
      <p class="tamanoLetra"><div class="dropdown tamanoLetra">VACANTES<div class="dropdown-content">
        </div>
        </div>
      </p>

</a>
  <a href="javascript:void(0);" style="font-size:15px;" class="icon" onclick="myFunction()">&#9776;</a>
</div>

  
</div>     
    
</nav>
