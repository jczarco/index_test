<footer>
    <div id="footer" class="w3-row w3-border">
       
            <div id="Social" class="w3-container w3-half">
                
                    <a target="_blank" href="https://www.facebook.com/REVIMEXOFICIAL/">
                        <img class="imageFooter" alt="Facebook" src="images/CONTACT-PROPIEDADES-WEB-FACEBOOK.png" >
                    </a>
                
                    <a target="_blank" href="https://twitter.com/oficialrevimex">
                        <img class="imageFooter" alt="Twitter" src="images/CONTACT-PROPIEDADES-WEB-TWITTER.png" >
                    </a>

              
                    <a target="_blank" href="https://www.instagram.com/revimex_oficial/">
                        <img class="imageFooter" alt="Instagram" src="images/CONTACT-PROPIEDADES-WEB-INSTAGRAM.png" >
                    </a>
                
                    <a href="https://www.youtube.com/channel/UCdn2VMwAvrJ_Te9nJqFYHYg/videos" target="_blank">
                        <img class="imageFooter" alt="YouTube" src="images/CONTACT-PROPIEDADES-WEB-YOUTUBE.png" >
                    </a>
                
            </div>
            <div id="aviso" class="w3-container w3-half">
                
                    <p>
                        <a href="http://www.revimex.mx/sitio/aviso-privacidad.php" target="_blank"> Aviso de Privacidad </a>
                        <br>
                        <a href="http://www.revimex.mx/sitio/faqs.php" target="_blank">FAQS</a> | <a href="http://www.revimex.mx/sitio/equipo.php" target="_blank">Equipo</a> | REVIMEX 01 800 200 0440
                    </p>
                
            </div>
        
    </div>
</footer>