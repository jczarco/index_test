/**
 * Created by maquina81 on 03/07/17.
 */

// Expresion regular para obtener extension de archivo
var re = /(?:\.([^.]+))?$/;

/*
 * True si el tamaño de pantalla es el de un móvil
 */
function isMobile() {
    return $(window).width() <= 800;
}

/*
 + Construe el modal para una propiedad
 */
 var mrkrPlace = '';
function createModal(propiedad, index) {
  mrkrPlace = false;
    $("#description-casas, #description-casas-mobile").html("");

    var modal_size = "col-md-6";

    if ( isMobile() ) {
        modal_size = "col-md-12";
    }

    var modal_casa = '<div class="' + modal_size + ' hola_description" id="house_description_' + index + '" style="display: block">' +

        '<div class="col-md-12 carr">' +
        '<div id="myCarousel_' + index + '" class="carousel slide" data-ride="carousel">' +
        '<!-- Indicators -->' +
        '<ol class="carousel-indicators">';

    var main_photo = "images/700x420.jpg";
    if (!jQuery.isEmptyObject(propiedad.fotoPrincipal)) {
        main_photo = propiedad.fotoPrincipal;
    }

    if (propiedad.files.length > 0) {
        var ind = 0;
        propiedad.files.forEach(function (file) {
            if (re.exec(file.nombre)[1] != "mp4") {
                if (ind === 0) {
                    modal_casa = modal_casa + '<li data-target="#myCarousel_' + index + '" data-slide-to="0" class="active"></li>';
                } else {
                    modal_casa = modal_casa + '<li data-target="#myCarousel_' + index + '" data-slide-to="' + ind + '"></li>';
                }
                ind += 1;
            }

        });
    } else {
        modal_casa = modal_casa + '<li data-target="#myCarousel_' + index + '" data-slide-to="0" class="active"></li>';
    }


    modal_casa = modal_casa +
        '</ol>' +

        '<!-- CorazÃ³n de Like -->' +
        '<div style="cursor: pointer; position: absolute; z-index: 4; padding-right: : 10px; padding-top: 15px; margin-left: 90%">' +
        '<img height="13%" id="heart_' + index + '" src="images/ICON-PIC-HEART.png" "></i>' +
        '</div>' +

        '<!-- Regreso a Cards -->' +
        '<div style="cursor: pointer; position: absolute; z-index: 4; padding-left: 15px; padding-top: 15px;">' +
        '<img height="13%" id="card_cubes_' + index + '" src="images/ICON-PIC-RETURN.png" "></i>' +
        '</div>' +

        '<!-- Wrapper for slides -->' +
        '<div class="carousel-inner">' +

        '<div class="captionFav" id="modalFavoritos' + index + '" style="display:none;">' +
        '<center>' +
        '<br>' +
        '<button id = "GoToFavorites_' + index + '" class="botonFav">Ir a favoritos</button>' +
        '<br>' +
        '<br>' +
        '<br>' +
        '<button id = "KeepWhatching_' + index + '" class="botonFav">Seguir viendo</button>' +
        '</center>' +
        '</div>';

    if (propiedad.files.length > 0) {
        ind = 0;
        propiedad.files.forEach(function (file) {
            if (re.exec(file.nombre)[1] != "mp4") {
                if (ind === 0) {
                    modal_casa = modal_casa +
                        '<div class="item active">' +
                        '<center>' +
                        '<img class="imgt" src="' + file.linkPublico + '" alt="' + file.nombre + '">' +
                        '<center>' +
                        '<div class="carousel-caption">' +
                        '</div>' +
                        '</div>';
                } else {
                    modal_casa = modal_casa +
                        '<div class="item">' +
                        '<center>' +
                        '<img class="imgt" src="' + file.linkPublico + '" alt="' + file.nombre + '">' +
                        '</center>' +
                        '<div class="carousel-caption">' +
                        '</div>' +
                        '</div>';
                }
                ind += 1;


            }
        });
    } else {
        modal_casa = modal_casa +
            '<div class="item active">' +
            '<center>' +

            '<img class="imgt" src="images/700x420.jpg" alt="Imagen no disponible">' +
            '<center>' +
            '<div class="carousel-caption">' +
            '</div>' +
            '</div>';
    }


    modal_casa = modal_casa +
        '</div>' +
        '<!-- Left and right controls -->' +
        '<a class="left carousel-control" href="#myCarousel_' + index + '" data-slide="prev">' +
        '<span class="glyphicon glyphicon-chevron-left"></span>' +
        '<span class="sr-only">Anterior</span>' +
        '</a>' +

        '<a class="right carousel-control" href="#myCarousel_' + index + '" data-slide="next">' +
        '<span class="glyphicon glyphicon-chevron-right"></span>' +
        '<span class="sr-only">Siguiente</span>' +
        '</a>' +
        '</div>' +
        '<center>' +
        '<div class="revimexBlue"><br>' +

        '<i class="capitalize" style="font-size: 120%">' + nombreMunicipio(propiedad.Municipio__c) + propiedad.Estado__c + '<br>' +
        '' + propiedad.Calle__c +
        ', Col. ' + propiedad.Colonia__c + '<br>' +
        '</i>' +
        '</div>' +
        '</center>' +
        '</div>' +

        '<div class="row" align="center">' +
        '<div class="col-md-4 col-md-offset-4 detelle">' +
        '<button class="boton_detalles_auto"  class="btn btn-primary" data-toggle="modal" id="back_to_' + index + '" style="    margin-bottom: 4%;">Regresar</button>' +
        '</div>' +
        '</div>' +

        // INFORMACION CASAS
        '<div class="row revimexBlue">' +
        // Caracteristicas
        '<div class="col-md-6"><br>' +

        '<div class="row">' +
        '<p class="bg-primary" align="center">Características</p>' +
        '<ul><br>' +
        '<li><strong>ID: </strong>' + propiedad.oferta + '</li>' +
        '<li><strong>Precio:</strong> $' + propiedad.PrecioVenta__c + '</li>' +

        '<li><div><strong> Terreno:</strong> ' + propiedad.Terreno_m2__c + '&nbsp;m<sup>2</sup></div>' + '</li>' +
        '<li><strong>Construcción:</strong> ' + propiedad.Construccion_m2__c + '&nbsp;m<sup>2</sup>' + '</li>' +
        '<li><strong>Niveles:</strong> ' + propiedad.Niveles_Plantas__c + '</li>' +

        '<li><strong>Habitaciones: </strong>' + propiedad.N_de_Habitaciones__c + '</li>' +
        '<li><strong>Baños: </strong>' + propiedad.N_de_Ba_os__c + '</li>' +
        '<li><strong>Estacionamientos: </strong> ' + propiedad.Estacionamiento__c + '</li>' +

        '</ul>' +
        '</div>' +

        '<div class="row" align="center">' +
        '<div class="col-md-4">' +

        '</div>' +
        '<div class="col-md-4">' +

        '</div>' +
        '<div class="col-md-4">' +

        '</div>' +
        '</div>' +
        '<br><br>' +
        '</div>' +

        // Lugares Cercanos
        '<div class="col-md-6" align="center"><br>' +
        '<p class="bg-primary">Lugares cercanos</p>' +
        '<br>' +
        '<div class="row" align="center">' +     
        '<div class="col-xs-4 col-md-4">' +
        '<button id="cormercio_' + index + '" class="btn btn-default"><img id="img_cormercio_' + index + '" class="img_borde" class="IconTarjeta" src="images/IconoTarjetaDinamica/ICONS-PROPIEDADES-WEB_COMERCIALES-OFF.png"></button><i class="numTarjeta"></i>' +
        '</div>' +
        '<div class="col-xs-4 col-md-4">' +
        '<button id="escuelas_' + index + '" class="btn btn-default"><img id="img_escuelas_' + index + '" class="img_borde" class="IconTarjeta" src="images/IconoTarjetaDinamica/ICONS-PROPIEDADES-WEB_ESCUELAS-OFF.png"></button><i class="numTarjeta"></i>' +
        '</div>' +
        '<div class="col-xs-4 col-md-4">' +
        '<button id="super_' + index + '" class="btn btn-default"><img id="img_super_' + index + '" class="img_borde" class="IconTarjeta" src="images/IconoTarjetaDinamica/ICONS-PROPIEDADES-WEB_SUPER-OFF.png"></button><i class="numTarjeta"></i>' +
        '</div>' +
        '</div>' +
        
        '</br></br>' +
        
        '<div class="row" align="center">' +
        '<div class="col-xs-4 col-md-4">' +
        '<button id="hospitales_' + index + '" class="btn btn-default"><img id="img_hospitales_' + index + '" class="img_borde" class="IconTarjeta" src="images/IconoTarjetaDinamica/ICONS-PROPIEDADES-WEB_HOSPITALES-OFF.png"></button><i class="numTarjeta"></i>' +
        '</div>' +
        '<div class="col-xs-4 col-md-4">' +
        '<button id="restaurantes_' + index + '" class="btn btn-default"><img id="img_restaurantes_' + index + '" class="img_borde" class="IconTarjeta" src="images/IconoTarjetaDinamica/ICONS-PROPIEDADES-WEB_RESTAURANTES-OFF.png"></button><i class="numTarjeta"></i>' +
        '</div>' +
        '<div class="col-xs-4 col-md-4">' +
        '<button id="parques_' + index + '" class="btn btn-default"><img id="img_parques_' + index + '" class="img_borde" class="IconTarjeta" src="images/IconoTarjetaDinamica/ICONS-PROPIEDADES-WEB_PARQUES-OFF.png"></button><i class="numTarjeta"></i>' +
        '</div>' +
        '</div>' +
        '</div>' +
        '</div>' +

        '<div class="row" align="center">' +
        '<div class="col-md-4 col-md-offset-4 detalle">' +
        '<button class="boton_detalles_auto"  class="btn btn-primary" onclick="modal_variables(\'' +
        propiedad.Calle__c +
        '\', \'' + propiedad.Colonia__c +
        '\', \'' + propiedad.Municipio__c +
        '\', \'' + propiedad.Estado__c +
        '\', \'' + propiedad.Terreno_m2__c +
        '\', \'' + propiedad.Construccion_m2__c +
        '\', \'' + propiedad.N_de_Habitaciones__c +
        '\', \'' + propiedad.N_de_Ba_os__c +
        '\', \'' + propiedad.Patios__c +
        '\', \'' + propiedad.Estacionamiento__c +
        '\', \'' + propiedad.PrecioVenta__c +
        '\', \'' + main_photo +
        '\', \'' + propiedad.oferta +
        '\', \'' + propiedad.Punto_Latitude__c +
        '\', \'' + propiedad.Punto_Longitude__c +
        '\', \'' + propiedad.Pros__c +
        '\', \'' + propiedad.TiempoOferta__c +
        '\', \'' + propiedad.Id_Colonia +
        '\', \'' + propiedad.Contras__c +   
        '\', \'' + propiedad.ValorReferencia__c  + 
        '\', \'' + propiedad.Precio_Original__c  +
        '\', \'' + propiedad.Escritu__c  +
        '\', \'' + propiedad.Estatus_de_invasion__c  +   
        '\', \'' + propiedad.EtapaDesalExtraJud__c  +    
        '\', \'' + propiedad.EtapaDesalojoJudicial__c  +   
        '\', \'' + propiedad.Corte_de_Suministros__c  + 


        '\');" data-toggle="modal" data-target="#pdf-modal" >Detalles</button>' +
        '</div>' +
        '</div>' +

        '</br></br>' +

        //SECCION GERENTE
        '<div class="row">' +
        '<div class="col-md-12 gerente">' +
        '<p class="bg-primary" align="center">Ejecutivo de Ventas</p>' +
        '<div class="row revimexBlue" style="margin-top: -4%;">' +
        '<br><div id="msg_form"></div>' +
        '<div class="col-md-7" class="res-ventas">' +
            
            '<div class="col-xs-12">' +
                '<p class="ventas-detalle">' +
                    '<b><img id="heart" class="logos-rs" src="images/CONTACT-PROPIEDADES-WEB-MAIL.png">&nbsp; &nbsp;<a href="mailto:info@revimex.mx" style="font-size:.78em; text-transform:lowercase;font-size: 16px;">info@revimex.mx</a></b><br><br>' +
                '</p>' +
            '</div>' +

            '<div class="col-xs-12">' +
                '<p class="ventas-detalle">' +
                    '<b><img id="heart" class="logos-rs" src="images/CONTACT-PROPIEDADES-WEB-PHONE.png">&nbsp; &nbsp;01 800 200 0440</b><br><br>' +
                '</p>' +
            '</div>' +  

            '<div class="col-xs-12" align="center">' +
                '<div class="ventas-detalleII">' +

                    '<div class="col-xs-3" align="center">' +
                        '<b><a target="_blank" href="https://www.facebook.com/REVIMEXOFICIAL/"><img id="facebook" class="logos-rs" src="images/CONTACT-PROPIEDADES-WEB-FACEBOOK.png""></a>' +
                    '</div>' + 

                    '<div class="col-xs-3" align="center">' +
                        '<b><a target="_blank" href="https://www.instagram.com/revimex_oficial/"><img id="instagram" class="logos-rs" src="images/CONTACT-PROPIEDADES-WEB-INSTAGRAM.png""></a>' +
                    '</div>' +

                    '<div class="col-xs-3" align="center">' +
                        '<b><a target="_blank" href="https://twitter.com/oficialrevimex"><img id="twitter" class="logos-rs" src="images/CONTACT-PROPIEDADES-WEB-TWITTER.png""></a>' +
                    '</div>' +

                    '<div class="col-xs-3" align="center">' +
                        '<b><a target="_blank" href="https://www.youtube.com/channel/UCdn2VMwAvrJ_Te9nJqFYHYg/videos"><img id="youtube" class="logos-rs" src="images/CONTACT-PROPIEDADES-WEB-YOUTUBE.png""></a>' +
                    '</div>' +

                '</div>' +
            '</div>' + 

            '<br>' +
        '</div>' +

        '<div class="col-md-5">' +

        '<div class="col-xs-12">' +
        '<form id="form_EjecutivoVtas">' +
        '<div class="form-group">' +
        '<br><br>' +
        '<input type="text" class="input_borde" class="form-control" id="form_nombre_' + propiedad.Id + '" placeholder="  Nombre" aria-describedby="sizing-addon2" maxlength="140">' +
        '<br></br>' +
        '<input type="text" class="input_borde" class="form-control" id="form_telefono_' + propiedad.Id + '" placeholder="  Teléfono" aria-describedby="sizing-addon2" maxlength="10">' +
        '<br></br>' +
        '<input type="text" class="input_borde" class="form-control" placeholder="  E-mail" id="form_email_' + propiedad.Id + '" aria-describedby="sizing-addon2">' +
        '<br></br></br>' +
        '<textarea class="input_borde" class="form-control" placeholder="  Comentarios" id="form_mensaje_' + propiedad.Id + '" maxlength="900"></textarea><br>' +
        '</div>' +
        '</form>' +
        '</div>' +
        '</div>' +
        '<br>' +
        '</div>' +
        '</div>' +
        '</div>' +

        '<div class="row" align="center">' +
        '<div class="col-md-4 col-md-offset-4">' +
        '<button type="button" onclick="sendMail(\'' + propiedad.Id + '\');" class ="boton_detalles_auto" class="btn btn-primary">Enviar</button>' +
        '</div>' +
        '</div>' +

        '</br>';

    if ( isMobile() ) {
        $("#description-casas-mobile").append(modal_casa);
    } else {
        $("#description-casas").append(modal_casa);
    }

    // Si ya esta en favoritos le cambia el color del corazón
    isFavorite(propiedad.Id, index);

    modalListeners(index);

}

/*
 * Agrega una coma al nombre del estado
 */
function nombreMunicipio(estado) {
    if (estado == '') {
        return ''
    } else {
        return estado + ', '
    }
}

/*
 * Activa los listeners del modal
 */
function modalListeners(index) {
        $("#card_cubes_" + index + ", #back_to_" + index).click(function () {
            // Sale de la vista de Street maps
            map.getStreetView().setVisible(false);

            // Resetea el slider
            slider.noUiSlider.updateOptions({
                start: [0, 1500000]
            });

            $('#casas').appendTo('#house_cards');
            $("#house_description_" + index).hide();
            $("#house_cards").show();
             restoreMarkers();
            $('#casas_cercanas').hide();
            setDefaulBehaviorMarkers(propiedades);
          
            $("#titulocercanas").hide();
            
            stateCenter(index);
            /*
            if(map.getZoom() == 17){
                stateCenter(index);
            } else {
                reCentrar();
            }
            */

          
            $("#modalFavoritos" + index).hide();

            if (markesrsSerives.length > 0) {
                for (var i = 0; i < markesrsSerives.length; i++) {
                    markesrsSerives[i].setMap(null);
                }
            }

                       
            stopOthersMarkers();
            stopOthersClickedMarkers();
            slider.removeAttribute('disabled');
        });

        $("#heart_" + index).click(function () {
            iLikeIt(index);
        });

        $("#escuelas_" + index).click(function () {
            if (markesrsSerives.length > 0) {
              if (mrkrPlace == 1) {
                  clearServicesAndReCenter(index);
              } else {
                clearServicesAndReCenter(index);
                getMarkersPlace(index, 1);
                changeButtonColor(this.id, index);
              }
            } else {
              changeButtonColor(this.id, index);
              getMarkersPlace(index, 1);
            }
        });

        $("#restaurantes_" + index).click(function () {
            if (markesrsSerives.length > 0) {
              if (mrkrPlace == 2) {
                  clearServicesAndReCenter(index);
              } else {
                clearServicesAndReCenter(index);
                getMarkersPlace(index, 2);
                changeButtonColor(this.id, index);
              }
            } else {
              changeButtonColor(this.id, index);
              getMarkersPlace(index, 2);
            }
        });

    //barrido

        $("#hospitales_" + index).click(function () {
            if (markesrsSerives.length > 0) {
              if (mrkrPlace == 5) {
                  clearServicesAndReCenter(index);
              } else {
                clearServicesAndReCenter(index);
                getMarkersPlace(index, 5);
                changeButtonColor(this.id, index);
              }
            } else {
              changeButtonColor(this.id, index);
              getMarkersPlace(index, 5);
            }
        });

        $("#cormercio_" + index).click(function () {
            if (markesrsSerives.length > 0) {
              if (mrkrPlace == 6) {
                  clearServicesAndReCenter(index);
              } else {
                clearServicesAndReCenter(index);
                getMarkersPlace(index, 6);
                changeButtonColor(this.id, index);
              }
            } else {
              changeButtonColor(this.id, index);
              getMarkersPlace(index, 6);
            }
        });

        $("#parques_" + index).click(function () {
            if (markesrsSerives.length > 0) {
              if (mrkrPlace == 7) {
                  clearServicesAndReCenter(index);
              } else {
                clearServicesAndReCenter(index);
                getMarkersPlace(index, 7);
                changeButtonColor(this.id, index);
              }
            } else {
                changeButtonColor(this.id, index);
                getMarkersPlace(index, 7);
            }
        });

        $("#super_" + index).click(function () {
            if (markesrsSerives.length > 0) {
              if (mrkrPlace == 8) {
                  clearServicesAndReCenter(index);
              } else {
                clearServicesAndReCenter(index);
                getMarkersPlace(index, 8);
                changeButtonColor(this.id, index);
              }
            } else {
                changeButtonColor(this.id, index);
                getMarkersPlace(index, 8);
            }
        });

        $("#GoToFavorites_" + index).click(function () {
            window.location.href = 'favoritos.html';
        });

        $("#KeepWhatching_" + index).click(function () {
            $("#modalFavoritos" + index).hide();
        });
}

function clearServicesAndReCenter(id) {
    offAllButtons(id); // Devuelve los botones a su color original

    for (var i = 0; i < markesrsSerives.length; i++) {
        markesrsSerives[i].setMap(null);
    }

    markesrsSerives = [];
    try {
        for (i = 0; i < allMarkers.length; i++) {
        
        var ij = "marker" + id;

        if (ij == allMarkers[i].id) {
            map.setZoom(17);
            map.setCenter(allMarkers[i].getPosition());
        }
    }
    } catch (error) {
        console.log("Error: " + error.message());
    }
    
}

function offAllButtons(index) {
    $("#img_restaurantes_" + index).attr("src", "images/IconoTarjetaDinamica/ICONS-PROPIEDADES-WEB_RESTAURANTES-OFF.png");
    $("#img_escuelas_" + index).attr("src", "images/IconoTarjetaDinamica/ICONS-PROPIEDADES-WEB_ESCUELAS-OFF.png");
    $("#img_hospitales_" + index).attr("src", "images/IconoTarjetaDinamica/ICONS-PROPIEDADES-WEB_HOSPITALES-OFF.png");
    $("#img_cormercio_" + index).attr("src", "images/IconoTarjetaDinamica/ICONS-PROPIEDADES-WEB_COMERCIALES-OFF.png");
    $("#img_super_" + index).attr("src", "images/IconoTarjetaDinamica/ICONS-PROPIEDADES-WEB_SUPER-OFF.png");
    $("#img_parques_" + index).attr("src", "images/IconoTarjetaDinamica/ICONS-PROPIEDADES-WEB_PARQUES-OFF.png");
}

function changeButtonColor(id, index) {
    if ("restaurantes_" + index == id) {
        $("#img_restaurantes_" + index).attr("src", "images/IconoTarjetaDinamica/ICONS-PROPIEDADES-WEB_RESTAURANTES-ON.png");
    } else {
        $("#img_restaurantes_" + index).attr("src", "images/IconoTarjetaDinamica/ICONS-PROPIEDADES-WEB_RESTAURANTES-OFF.png");
    }

    if ("escuelas_" + index == id) {
        $("#img_escuelas_" + index).attr("src", "images/IconoTarjetaDinamica/ICONS-PROPIEDADES-WEB_ESCUELAS-ON.png");
    } else {
        $("#img_escuelas_" + index).attr("src", "images/IconoTarjetaDinamica/ICONS-PROPIEDADES-WEB_ESCUELAS-OFF.png");
    }

    if ("hospitales_" + index == id) {
        $("#img_hospitales_" + index).attr("src", "images/IconoTarjetaDinamica/ICONS-PROPIEDADES-WEB_HOSPITALES-ON.png");
    } else {
        $("#img_hospitales_" + index).attr("src", "images/IconoTarjetaDinamica/ICONS-PROPIEDADES-WEB_HOSPITALES-OFF.png");
    }

    if ("cormercio_" + index == id) {
        $("#img_cormercio_" + index).attr("src", "images/IconoTarjetaDinamica/ICONS-PROPIEDADES-WEB_COMERCIALES-ON.png");
    } else {
        $("#img_cormercio_" + index).attr("src", "images/IconoTarjetaDinamica/ICONS-PROPIEDADES-WEB_COMERCIALES-OFF.png");
    }

    if ("super_" + index == id) {
        $("#img_super_" + index).attr("src", "images/IconoTarjetaDinamica/ICONS-PROPIEDADES-WEB_SUPER-ON.png");
    } else {
        $("#img_super_" + index).attr("src", "images/IconoTarjetaDinamica/ICONS-PROPIEDADES-WEB_SUPER-OFF.png");
    }

    if ("parques_" + index == id) {
        $("#img_parques_" + index).attr("src", "images/IconoTarjetaDinamica/ICONS-PROPIEDADES-WEB_PARQUES-ON.png");
    } else {
        $("#img_parques_" + index).attr("src", "images/IconoTarjetaDinamica/ICONS-PROPIEDADES-WEB_PARQUES-OFF.png");
    }
}

function modal_variables(calle, colonia, municipio, estado, terreno, constru, habitaciones, banos, 
    patios, estacionamientos, precio, imagen, folio, Punto_Latitude__c, Punto_Longitude__c, Pros__c, 
    TiempoOferta__c, Id_Colonia, Contras__c, ValorReferencia__c, Precio_Original__c, Escritu__c,
    Estatus_de_invasion__c, EtapaDesalExtraJud__c, EtapaDesalojoJudicial__c, Corte_de_Suministros__c) {
    $("#pdf-modal").html("");
    // si el valor de folio es null -> el campo debe de estar vacÃ¬o s
    if (folio == 'null') {
        folio = '';
    }


    //Convertimos valores string de la BD a enteros para darles formato

     ValorReferencia__c = parseInt(ValorReferencia__c);
     Precio_Original__c = parseInt(Precio_Original__c);

     //Metodo para dar formato a los enteros
    function formatNumber(number)
    {
        number = number.toFixed(2) + '';
        x = number.split('.');
        x1 = x[0];
        x2 = x.length > 1 ? '.' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + ',' + '$2');
        }
        return x1;
    }

    //Validación de pros
    Pros__c = Pros__c.split('; ');

    var pro0, pro1, pro2, pro3;

    if (Pros__c[0] == null) {
        pro0 = '';
    }else{
        pro0 = Pros__c[0];
    }

    if (Pros__c[1] == null) {
        pro1 = '';
    }else{
        pro1 = Pros__c[1];
    }

    if (Pros__c[2] == null) {
        pro2 = '';
    }else{
        pro2 = Pros__c[2];
    }

    if (Pros__c[3] == null) {
        pro3 = '';
    }else{
        pro3 = Pros__c[3];
    }

    //Validación de contras

    Contras__c = Contras__c.split('; ');

    var con0, con1, con2, con3;

    if (Contras__c[0] == null) {
        con0 = '';
    }else{
        con0 = Contras__c[0];
    }

    if (Contras__c[1] == null) {
        con1 = '';
    }else{
        con1 = Contras__c[1];
    }

    if (Contras__c[2] == null) {
        con2 = '';
    }else{
        con2 = Contras__c[2];
    }

    if (Contras__c[3] == null) {
        con3 = '';
    }else{
        con3 = Contras__c[3];
    }

        //Ajax para sacar datos de la tabla de colonias

        
        
        $.ajax({
             url: url + "/propiedades/searchCol",
             type: "POST",
             data: {
                 folioColonia: Id_Colonia
             },
             dataType: "JSON",

             success: function (respuesta) {

                //console.log(respuesta["colonia"][0].Precio_por_m2_construccion_mercado__c);
                var pcoloniaAjax = ((respuesta["colonia"][0].Precio_por_m2_construccion_mercado__c)*constru);

                var ahorro = Math.round(((respuesta["colonia"][0].Precio_por_m2_construccion_mercado__c - (ValorReferencia__c/constru))/respuesta["colonia"][0].Precio_por_m2_construccion_mercado__c)*100);

                var vendidas = respuesta["colonia"][0].Vendidas__c;    

                var disponibles = respuesta["colonia"][0].Propiedades_Disponibles__c;

                var califSuper = respuesta["colonia"][0].Calificacion_super__c

                var califParques = respuesta["colonia"][0].Calificacion_parques__c

                var califCentro = respuesta["colonia"][0].Calificacion_centros_comerciales__c

                var califHospitales = respuesta["colonia"][0].Calificacion_hospitales__c

                var califescuelas = respuesta["colonia"][0].Calificacion_escuelas__c

                var califRestauranes = respuesta["colonia"][0].Calificacion_restaurantes__c

                var califTotal = respuesta["colonia"][0].Total_Calificacion__c

                var suma = califTotal

                //console.log(disponibles);

                $('#pcolonia').text(formatNumber(pcoloniaAjax));

                $('#ahorro').text(ahorro);   

                $('#vendidas').text(vendidas);

                $('#disponibles').text(disponibles);

                //numero de imagenes de una calificación
                //Centros Super
                for (var i = 1; i <= califSuper; i++) {
                    $('#M' + i).attr('src','images/iconoModal/festrella-on.png');
                }
                //Centros Parques
                for (var i = 1; i <= califParques; i++) {
                    $('#P' + i).attr('src','images/iconoModal/festrella-on.png');
                }
                //Centros Centro
                for (var i = 1; i <= califCentro; i++) {
                    $('#C' + i).attr('src','images/iconoModal/festrella-on.png');
                }
                //Hospitales
                for (var i = 1; i <= califHospitales; i++) {
                    $('#H' + i).attr('src','images/iconoModal/festrella-on.png');
                }
                //Escuelas
                for (var i = 1; i <= califescuelas; i++) {
                    $('#E' + i).attr('src','images/iconoModal/festrella-on.png');
                }
                //Restauranes
                for (var i = 1; i <= califRestauranes; i++) {
                    $('#R' + i).attr('src','images/iconoModal/festrella-on.png');
                }


                //Calificación total
                if (califTotal > 28) {
                    califTotal = "MUY BUENA";
                }else if (califTotal > 21) {
                    califTotal = "BUENA";
                }else if (califTotal > 14) {
                    califTotal = "REGULAR";
                }else if (califTotal > 7) {
                    califTotal = "MUY MALA";
                }else{

                }

                $('#califTotal').text(califTotal);

                $('#suma').text(suma);


                //obj = JSON.parse(respuesta);
             },
             error: function (respuesta) {
                 console.log(respuesta);
             },


         /*}).done(function(pcoloniaAjax){
            $('#pcolonia').text(pcoloniaAjax);
         */});
            
          //console.log($('#uno').attr('calf'));


        //Validación escrituras

        if (Escritu__c == "Con Escrituras") {
            Escritu__c = "Sí";
        }else{
            Escritu__c = "No";
        }

        //Validación desalojada
        if (Estatus_de_invasion__c == "Desalojada") {
            Estatus_de_invasion__c = "Sí";
        }else{
            Estatus_de_invasion__c = "No";
        }

    //console.log(Id_Colonia);

    if (calle != '') {

        calle = calle+',';
    }

    if (colonia != '') {

        colonia = colonia+',';
    }

    if (municipio != '') {

        municipio = municipio+',';
    }


    

    $("#pdf-modal").append('<div class="modal-dialog-detalles">' +
            '<div class="modal-content-detalles">' +
                '<div class="modal-body">' +

                '<div id="pdf" class="div">' +

                    '<div class="row" style="margin-top:6px;">' +
                        '<div class="col-xs-12 col-md-6" style="padding: 0px 6px;">' +
                            '<div class="row azulRevimex" align="center">' +
                                '<img  class="revimexImagen" src="images/revimex-logo.png">' +
                            '</div>' +

                        '</div>' +

                        '<div class="col-xs-12 col-md-3" style="padding: 0px 6px;">' +
                            '<div class="col-xs-6 col-md-12 arenaRevimex mod-resp" align="center" style="height:82px; padding-top: 9px;">' +
                                '<span class="titulo-resp">FICHA TÉCNICA COMERCIAL</span>' +
                            '</div>' +
                        '</div>' +

                        '<div class="col-xs-12 col-md-3" style="padding: 0px 6px;">' +
                            '<div class="col-xs-6 col-md-12 mod-resp" align="center" style="background: #012967; height:82px; padding: 26px;">' +
                                '<span class="titulo-resp" style="color:white;"><img style="width: 19%;" src="images/iconoModal/pros.png"> PROS</span>' +
                            '</div>' +
                        '</div>' +

                    '</div>' +

                    '<div class="separador"></div>' +

                    '<div class="row">' +
                        '<div class="col-xs-12 col-md-6" style="padding-left: 6px; padding-top: 6px">' +
                            '<img class="imagen-modal-detalles" src="' + imagen + '">' +
                        '</div>' +

                        '<div class="col-xs-12 col-md-3" style="padding: 6px 6px;">' +
                            '<div class="row arenaClara">' +
                                '<br>' +
                                '<p class="direccion" align="center" style="font-size: 22px;">' + folio +
                                
                                '</p>' +
                            '</div>' +

                            '<div class="row arenaClara" style="margin-top: 12px; height: 204px; vertical-align: middle;">' +
                                
                                
                              
                                '<p class="descripcion" style="text-align: center; margin-top: 12px;">'+ calle +
                                ' '+ colonia + ' <span style="font-weight: bold;">' + municipio + ' ' + estado + 
                                '</span></p>' +
                                
                            '</div>' +

                                '<div class="separador"></div>' +
                                
                                '<div class="row" style="background-color: #FFEFD8; margin-top: 12px;">' +
                                    '<p class="precio" align="center">' +
                                    '<img style="width: 7%;" src="images/iconoModal/flocation.png"><a style="font-size: 17px;" target="_blank" href="https://maps.google.com.mx/?q='+ Punto_Latitude__c + ',' + Punto_Longitude__c +'">LOCALIZADOR</a>'+
                                    '</p>' +
                                '</div>' +
                            '</div>' +

                            '<div class="col-xs-12 col-md-3" style="padding: 0px 0px;">' +

                            '<div class="row arenaClara" style="background-color: #E7E97D; margin-top: 6px; margin-left: 6px; margin-right: 6px; height: 78px;">' +
                                '<p class="direccion" align="center" style="font-size: 16px;">' + pro0 +
                                
                                '</p>' +
                            '</div>' +

                            '<div class="row arenaClara" style="background-color: #E7E97D; margin-top: 6px; margin-left: 6px; margin-right: 6px; height: 78px;">' +
                                '<p class="direccion" align="center" style="font-size: 16px;">' + pro1 +
                                
                                '</p>' +
                            '</div>' +

                            '<div class="row arenaClara" style="background-color: #E7E97D; margin-top: 6px; margin-left: 6px; margin-right: 6px; height: 78px;">' +
                                '<p class="direccion" align="center" style="font-size: 16px;">' + pro2 +
                                
                                '</p>' +
                            '</div>' +

                            '<div class="row arenaClara" style="background-color: #E7E97D; margin-top: 6px; margin-left: 6px; margin-right: 6px; height: 77px;">' +
                                '<p class="direccion" align="center" style="font-size: 16px;">' + pro3 +
                                
                                '</p>' +
                            '</div>' +
                            
                    '</div>' +

                    

                '</div>' +

                '<div class="row" style="margin-top:6px;">' +
                        '<div class="col-xs-12 col-md-9" style="padding: 0px 6px;">' +
                            '<div class="row" align="center">' +
                                '<div class="col-sm-4" style="border: 4px solid #4DB1E0; border-radius: 13px; padding: 12px; width:32%; margin-right: 16px;"><span class="direccion" style="font-size: 16px; font-weight: bold;"><img style="width: 10%;" src="images/iconoModal/tecnica-propiedad.png"> TÉCNICA PROPIEDAD</span></div>' +
                    
                                '<div class="col-sm-4" style="border: 4px solid #4DB1E0; border-radius: 13px; padding: 12px; width:32%; margin-right: 16px;"><span class="direccion" style="font-size: 16px; font-weight: bold;"><img style="width: 10%;" src="images/iconoModal/resumen-financiero.png"> RESUMEN FINANCIERO</span></div>' +

                                '<div class="col-sm-4" style="border: 4px solid #4DB1E0; border-radius: 13px; padding: 12px; width:32%;"><span class="direccion" style="font-size: 16px; font-weight: bold;"><img style="width: 10%;" src="images/iconoModal/desalojo.png"> DESALOJO</span></div>' +
                            '</div>' +

                        '</div>' +

                        '<div class="col-xs-12 col-md-3" style="padding: 0px 6px;">' +
                            '<div class="col-xs-6 col-md-12 mod-resp" align="center" style="background: #012967; height:82px; padding: 26px;">' +
                                '<span class="titulo-resp" style="color:white;"><img style="width: 19%;" src="images/iconoModal/contras.png"> CONS</span>' +
                            '</div>' +
                        '</div>' +

                '</div>' +


                '<div class="row" style="margin-top:6px;">' +
                        '<div class="col-xs-12 col-md-9" style="padding: 0px 6px; margin-top: -23px">' +
                            '<div class="row" align="center">' +


                                '<div class="col-sm-2">'+
                                '<div style="border: 2px solid #FFFFFF; background-color: #002E6D; width:122%; margin-right: 15px; margin-left: -15px;"><img style="width:22%;" src="images/iconoModal/fterreno2.png"></div><br>'+
                                '<div style="border: 2px solid #FFFFFF; background-color: #002E6D; width:122%; margin-right: 15px; margin-left: -15px; margin-top: -19px;"><img style="width:22%;" src="images/iconoModal/fconstruccion2.png"></div><br>'+
                                '<div style="border: 2px solid #FFFFFF; background-color: #002E6D; width:122%; margin-right: 15px; margin-left: -15px; margin-top: -19px;"><img style="width:22%;" src="images/iconoModal/fhabitaciones2.png"></div><br>'+
                                '<div style="border: 2px solid #FFFFFF; background-color: #002E6D; width:122%; margin-right: 15px; margin-left: -15px; margin-top: -19px;"><img style="width:22%;" src="images/iconoModal/fbanio2.png"></div><br>'+
                                '<div style="border: 2px solid #FFFFFF; background-color: #002E6D; width:122%; margin-right: 15px; margin-left: -15px; margin-top: -19px;"><img style="width:22%;" src="images/iconoModal/fpatio2.png"></div><br>'+
                                '<div style="border: 2px solid #FFFFFF; background-color: #002E6D; width:122%; margin-right: 15px; margin-left: -15px; margin-top: -19px;"><img style="width:22%;" src="images/iconoModal/festacionamiento2.png"></div><br>'+
                                '</div>' +

                                '<div class="col-sm-2">'+
                                '<div style="border: 2px solid #FFFFFF; background-color: #AEE1F3; margin-right: 18px; height: 32px; margin-left: -20px; width:127px;">'+ Math.round(terreno) +' m<sup>2</sup></div><br>' +
                                '<div style="border: 2px solid #FFFFFF; background-color: #AEE1F3; margin-right: 18px; height: 32px; margin-left: -20px; width:127px; margin-top: -19px;">'+ Math.round(constru) +' m<sup>2</sup></div><br>' +
                                '<div style="border: 2px solid #FFFFFF; background-color: #AEE1F3; margin-right: 18px; height: 32px; margin-left: -20px; width:127px; margin-top: -19px;">'+ habitaciones +' m<sup>2</sup></div><br>' +
                                '<div style="border: 2px solid #FFFFFF; background-color: #AEE1F3; margin-right: 18px; height: 32px; margin-left: -20px; width:127px; margin-top: -19px;">'+ banos +'</div><br>' +
                                '<div style="border: 2px solid #FFFFFF; background-color: #AEE1F3; margin-right: 18px; height: 32px; margin-left: -20px; width:127px; margin-top: -19px;">'+ patios +'</div><br>' +
                                '<div style="border: 2px solid #FFFFFF; background-color: #AEE1F3; margin-right: 18px; height: 32px; margin-left: -20px; width:127px; margin-top: -19px;">'+ estacionamientos +'</div><br>' +
                                '</div>'+
                    
                                '<div class="col-sm-2">'+
                                '<div style="border: 2px solid #FFFFFF; background-color: #002E6D; width:119%; margin-right: 15px; height: 33px; margin-left: -12px;"><span style="color: white">$ REFERENCIA</span></div>' +
                                '<div style="border: 2px solid #FFFFFF; background-color: #002E6D; width:119%; margin-right: 15px; height: 33px; margin-left: -12px;"><span style="color: white">$ ORIGINAL</span></div>' +
                                '<div style="border: 2px solid #FFFFFF; background-color: #002E6D; width:119%; margin-right: 15px; height: 33px; margin-left: -12px;"><span style="color: white">$ COLONIA</span></div>' +
                                '<div style="border: 2px solid #FFFFFF; background-color: #002E6D; width:119%; margin-right: 15px; height: 33px; margin-left: -12px;"><span style="color: white">AHORRO</span></div>' +
                                '<div style="border: 2px solid #FFFFFF; background-color: #002E6D; width:119%; margin-right: 15px; height: 33px; margin-left: -12px;"><span style="color: white">ESCRITURAS</span></div>' +
                                '<div style="border: 2px solid #FFFFFF; background-color: #002E6D; width:119%; margin-right: 15px; height: 33px; margin-left: -12px;"><span style="color: white">DESALOJADA</span></div>' +
                                '</div>'+
                                '<div class="col-sm-2">'+
                                '<div style="border: 2px solid #FFFFFF; background-color: #AEE1F3; margin-right: 18px; height: 32px; margin-left: -20px; width:127px;">'+formatNumber(ValorReferencia__c)+'</div><br>' +
                                '<div style="border: 2px solid #FFFFFF; background-color: #AEE1F3; margin-right: 18px; height: 32px; margin-left: -20px; width:127px; margin-top: -19px;">'+ formatNumber(Precio_Original__c) +'</div><br>' +
                                '<div style="border: 2px solid #FFFFFF; background-color: #AEE1F3; margin-right: 18px; height: 32px; margin-left: -20px; width:127px; margin-top: -19px;"><span id="pcolonia"></span></div><br>' +   
                                '<div style="border: 2px solid #FFFFFF; background-color: #AEE1F3; margin-right: 18px; height: 32px; margin-left: -20px; width:127px; margin-top: -19px;"><span id="ahorro"></span>%</div><br>' +
                                '<div style="border: 2px solid #FFFFFF; background-color: #AEE1F3; margin-right: 18px; height: 32px; margin-left: -20px; width:127px; margin-top: -19px;">' + Escritu__c + '</div><br>' +
                                '<div style="border: 2px solid #FFFFFF; background-color: #AEE1F3; margin-right: 18px; height: 32px; margin-left: -20px; width:127px; margin-top: -19px;">' + Estatus_de_invasion__c + '</div><br>' +
                                '</div>'+


                                '<div class="col-sm-2">'+
                                '<div style="border: 2px solid #FFFFFF; background-color: #002E6D; width:119%; margin-right: 15px; height: 33px; margin-left: -12px;"><span style="color: white">DÍAS STOCK</span></div>' +
                                '<div style="border: 2px solid #FFFFFF; background-color: #002E6D; width:119%; margin-right: 15px; height: 33px; margin-left: -12px;"><span style="color: white">EXTRAJUDICIAL</span></div>' +
                                '<div style="border: 2px solid #FFFFFF; background-color: #002E6D; width:119%; margin-right: 15px; height: 33px; margin-left: -12px;"><span style="color: white">JUDICIAL</span></div>' +
                                '<div style="border: 2px solid #FFFFFF; background-color: #002E6D; width:119%; margin-right: 15px; height: 33px; margin-left: -12px;"><span style="color: white">C. SUMINISTROS</span></div>' +
                                '<div style="border: 2px solid #FFFFFF; background-color: #002E6D; width:119%; margin-right: 15px; height: 33px; margin-left: -12px;"><span style="color: white">VENDIDAS</span></div>' +
                                '<div style="border: 2px solid #FFFFFF; background-color: #002E6D; width:119%; margin-right: 15px; height: 33px; margin-left: -12px;"><span style="color: white">DISPONIBLES</span></div>' +
                                '</div>'+
                                '<div class="col-sm-2">'+
                                '<div style="border: 2px solid #FFFFFF; background-color: #AEE1F3; margin-right: 18px; height: 32px; margin-left: -20px; width:133%;">'+TiempoOferta__c+'</div><br>' +
                                '<div style="border: 2px solid #FFFFFF; background-color: #AEE1F3; margin-right: 18px; height: 32px; margin-left: -20px; width:133%; margin-top: -19px;">' + EtapaDesalExtraJud__c + '</div><br>' +
                                '<div style="border: 2px solid #FFFFFF; background-color: #AEE1F3; margin-right: 18px; height: 32px; margin-left: -20px; width:133%; margin-top: -19px;">' + EtapaDesalojoJudicial__c + '</div><br>' +
                                '<div style="border: 2px solid #FFFFFF; background-color: #AEE1F3; margin-right: 18px; height: 32px; margin-left: -20px; width:133%; margin-top: -19px;">' + Corte_de_Suministros__c + '</div><br>' +
                                '<div style="border: 2px solid #FFFFFF; background-color: #AEE1F3; margin-right: 18px; height: 32px; margin-left: -20px; width:133%; margin-top: -19px;"><span id="vendidas"></span></div><br>' +    
                                '<div style="border: 2px solid #FFFFFF; background-color: #AEE1F3; margin-right: 18px; height: 32px; margin-left: -20px; width:133%; margin-top: -19px;"><span id="disponibles"></span></div><br>' +
                                '</div>'+


                            '</div>' +

                        '</div>' +

                        '<div class="col-xs-12 col-md-3" style="padding: 0px 6px;">' +

                            '<div class="col-xs-6 col-md-12 mod-resp" align="center" style="background: #DF9C96; height:82px; padding: 26px;">' +
                                '<span>'+con0+'</span>' +
                            '</div>' +

                            '<div class="col-xs-6 col-md-12 mod-resp" align="center" style="background: #DF9C96; height:82px; padding: 26px; margin-top: 9px;">' +
                                '<span>'+con1+'</span>' +
                            '</div>' +

                        '</div>' +


                        '<div class="row" style="margin-top:6px;">' +
                            '<div class="col-xs-12 col-md-9" style="padding: 0px 6px; margin-top: -14px;">' +
                                '<div class="row" align="center" style="border: 4px solid #4DB1E0; border-radius: 13px; padding: 12px; width:100%;">' +
                                    '<span class="direccion" style="font-size: 16px; font-weight: bold;"><img style="width: 3%;" src="images/iconoModal/servicios.png"> SERVICIOS DE LA ZONA</span>'+
                                '</div>' +

                                '<div class="col-sm-2" style="margin-top: 8px;">'+
                                '<div style="border: 2px solid #FFFFFF; width:122%; margin-right: 15px; margin-left: -15px;"><img style="width:30%;" src="images/IconoTarjetaDinamica/ICONS-PROPIEDADES-WEB_SUPER-OFF.png"></div><br>'+
                                '<div style="border: 2px solid #FFFFFF; width:122%; margin-right: 15px; margin-left: -15px; margin-top: -19px;"><img style="width:30%;" src="images/IconoTarjetaDinamica/ICONS-PROPIEDADES-WEB_PARQUES-OFF.png"></div><br>'+
                                '</div>' +

                                '<div class="col-sm-2" style="margin-top: 8px;">'+
                                '<div style="border: 2px solid #FFFFFF; background-color: #AEE1F3; margin-right: 18px; height: 44px; margin-left: -104px; width:211px;"><center>'+
                                '<span>'+
                                '<img id="M1" style="width: 15%;" src="images/iconoModal/festrella-off.png">'+
                                '<img id="M2" style="width: 15%;" src="images/iconoModal/festrella-off.png">'+
                                '<img id="M3" style="width: 15%;" src="images/iconoModal/festrella-off.png">'+
                                '<img id="M4" style="width: 15%;" src="images/iconoModal/festrella-off.png">'+
                                '<img id="M5" style="width: 15%;" src="images/iconoModal/festrella-off.png">'+
                                '</span>'+
                                '</center></div><br>' +
                                '<div style="border: 2px solid #FFFFFF; background-color: #AEE1F3; margin-right: 18px; height: 44px; margin-left: -104px; width:211px; margin-top: -19px;"><center>'+
                                '<span>'+
                                '<img id="P1" style="width: 15%;" src="images/iconoModal/festrella-off.png">'+
                                '<img id="P2" style="width: 15%;" src="images/iconoModal/festrella-off.png">'+
                                '<img id="P3" style="width: 15%;" src="images/iconoModal/festrella-off.png">'+
                                '<img id="P4" style="width: 15%;" src="images/iconoModal/festrella-off.png">'+
                                '<img id="P5" style="width: 15%;" src="images/iconoModal/festrella-off.png">'+
                                '</span>'+
                                '</center></div><br>' +
                                '</div>'+

                                '<div class="col-sm-2" style="margin-top: 8px;">'+
                                '<div style="border: 2px solid #FFFFFF; width:122%; margin-right: 15px; margin-left: -15px;"><img style="width:30%;" src="images/IconoTarjetaDinamica/ICONS-PROPIEDADES-WEB_COMERCIALES-OFF.png"></div><br>'+
                                '<div style="border: 2px solid #FFFFFF; width:122%; margin-right: 15px; margin-left: -15px; margin-top: -19px;"><img style="width:30%;" src="images/IconoTarjetaDinamica/ICONS-PROPIEDADES-WEB_HOSPITALES-OFF.png"></div><br>'+
                                '</div>' +

                                '<div class="col-sm-2" style="margin-top: 8px;">'+
                                '<div style="border: 2px solid #FFFFFF; background-color: #AEE1F3; margin-right: 18px; height: 44px; margin-left: -104px; width:211px;"><center>'+
                                '<span>'+
                                '<img id="C1" style="width: 15%;" src="images/iconoModal/festrella-off.png">'+
                                '<img id="C2" style="width: 15%;" src="images/iconoModal/festrella-off.png">'+
                                '<img id="C3" style="width: 15%;" src="images/iconoModal/festrella-off.png">'+
                                '<img id="C4" style="width: 15%;" src="images/iconoModal/festrella-off.png">'+
                                '<img id="C5" style="width: 15%;" src="images/iconoModal/festrella-off.png">'+
                                '</span>'+
                                '</center></div><br>' +
                                '<div style="border: 2px solid #FFFFFF; background-color: #AEE1F3; margin-right: 18px; height: 44px; margin-left: -104px; width:211px; margin-top: -19px;"><center>'+
                                '<span>'+
                                '<img id="H1" style="width: 15%;" src="images/iconoModal/festrella-off.png">'+
                                '<img id="H2" style="width: 15%;" src="images/iconoModal/festrella-off.png">'+
                                '<img id="H3" style="width: 15%;" src="images/iconoModal/festrella-off.png">'+
                                '<img id="H4" style="width: 15%;" src="images/iconoModal/festrella-off.png">'+
                                '<img id="H5" style="width: 15%;" src="images/iconoModal/festrella-off.png">'+
                                '</span>'+
                                '</center></div><br>' +
                                '</div>'+

                                '<div class="col-sm-2" style="margin-top: 8px;">'+
                                '<div style="border: 2px solid #FFFFFF; width:122%; margin-right: 15px; margin-left: -15px;"><img style="width:30%;" src="images/IconoTarjetaDinamica/ICONS-PROPIEDADES-WEB_ESCUELAS-OFF.png"></div><br>'+
                                '<div style="border: 2px solid #FFFFFF; width:122%; margin-right: 15px; margin-left: -15px; margin-top: -19px;"><img style="width:30%;" src="images/IconoTarjetaDinamica/ICONS-PROPIEDADES-WEB_RESTAURANTES-OFF.png"></div><br>'+
                                '</div>' +

                                '<div class="col-sm-2" style="margin-top: 8px;">'+
                                '<div style="border: 2px solid #FFFFFF; background-color: #AEE1F3; margin-right: 18px; height: 44px; margin-left: -104px; width:211px;"><center>'+
                                '<span>'+
                                '<img id="E1" style="width: 15%;" src="images/iconoModal/festrella-off.png">'+
                                '<img id="E2" style="width: 15%;" src="images/iconoModal/festrella-off.png">'+
                                '<img id="E3" style="width: 15%;" src="images/iconoModal/festrella-off.png">'+
                                '<img id="E4" style="width: 15%;" src="images/iconoModal/festrella-off.png">'+
                                '<img id="E5" style="width: 15%;" src="images/iconoModal/festrella-off.png">'+
                                '</span>'+
                                '</center></div><br>' +
                                '<div style="border: 2px solid #FFFFFF; background-color: #AEE1F3; margin-right: 18px; height: 44px; margin-left: -104px; width:211px; margin-top: -19px;"><center>'+
                                '<span>'+
                                '<img id="R1" style="width: 15%;" src="images/iconoModal/festrella-off.png">'+
                                '<img id="R2" style="width: 15%;" src="images/iconoModal/festrella-off.png">'+
                                '<img id="R3" style="width: 15%;" src="images/iconoModal/festrella-off.png">'+
                                '<img id="R4" style="width: 15%;" src="images/iconoModal/festrella-off.png">'+
                                '<img id="R5" style="width: 15%;" src="images/iconoModal/festrella-off.png">'+
                                '</span>'+
                                '</center></div><br>' +
                                '</div>'+




                            '</div>' +

                            '<div class="col-xs-12 col-md-3" style="padding: 0px 6px;">' +
                                '<div class="col-xs-6 col-md-12 mod-resp" align="center" style="background: #DF9C96;; height:82px; padding: 26px; margin-top: -14px;">' +
                                    con2+
                                '</div>' +
                            '</div>' +

                            '<div class="col-xs-12 col-md-3" style="padding: 0px 6px;">' +
                                '<div class="col-xs-6 col-md-12 mod-resp" align="center" style="background: #DF9C96;; height:82px; padding: 26px; margin-top: 8px;">' +
                                    con3+
                                '</div>' +
                            '</div>' +

                        '</div>' +


                        '<div class="row" style="margin-top:6px;">' +
                            '<div class="col-xs-12 col-md-9" style="padding: 0px 6px;">' +
                                '<div class="row" align="center">' +
                                    '<div class="col-sm-3" style="background-color: #002E6D; padding: 12px; margin-top: -20px; margin-right: 8px; width: 24%;"><span class="direccion" style="font-size: 16px; font-weight: bold; color: white;">CALIFICACIÓN</span></div>' +
                        
                                    '<div class="col-sm-3" style="background-color: #AEE1F3; padding: 12px; margin-top: -20px; margin-right: 8px; width: 24%;"><span id="califTotal" class="direccion" style="font-size: 16px; font-weight: bold;"></span></div>' +

                                    '<div class="col-sm-3" style="background-color: #002E6D; padding: 12px; margin-top: -20px; margin-right: 8px; width: 24%;"><span class="direccion" style="font-size: 16px; font-weight: bold; color: white;">TOTAL</span></div>' +

                                    '<div class="col-sm-3" style="background-color: #AEE1F3; padding: 12px; margin-top: -20px; margin-right: 8px; width: 24%;"><span id="suma" class="direccion" style="font-size: 16px; font-weight: bold;"></span></div>' +
                                '</div>' +

                            '</div>' +


                    '</div>' + 




                    '</div>' +                 

            '</div>' +

        '</div>' +
    '</div>');

}
