/**
 * Created by @HackeaMesta on 10/07/17.
 */
$(function() {
    var url = "https://laravelapi-achargoy.c9users.io"; 
        $( "#search" ).autocomplete({
         source: function (request, response) { 
                getPlaces(response); 
            },
          select: function(event,ui){
              document.cookie = "location=" + ui.item.value +"; Path=/;";
              window.location.href = 'sitio/mapas2.html';
          }
        });
        
        function getPlaces(response) {
            $.ajax({
                type: 'POST',
                url: url + "/propiedades/places",
                data: {
                    search: $("#search").val()
                },
                dataType: "JSON",
                success: function (data) {
                  response($.map(data.places, function(p){
                      var z = 0;
                       if (p.is_plaza == 1) {
                            z = 13;
                        } else {
                            z= 5;
                        }
                    return {
                        label: p.place,
                        value: p.place,
                        is_plaza: p.is_plaza,
                        zoom: z,
                        lat: p.latitude,
                        lng: p.longitude
                    };
                  }));
                }
            });
        }
});
