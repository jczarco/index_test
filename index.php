<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Revimex</title>





    <link href='https://fonts.googleapis.com/css?family=Comfortaa' rel='stylesheet'>
    <!-- Bootstrap Core CSS -->
    <link href="menu/css/bootstrap.css" rel="stylesheet">
    <link href="menu/css/full-slider.css" rel="stylesheet">
    <link href="menu/css/desplegar.css" rel="stylesheet">


    <link rel="stylesheet" href="menu/css/jquery-ui.css">

    <!-- Styles -->
    <link rel="stylesheet" href="menu/css/StyleSheet.css">
    <link rel="stylesheet" href="menu/css/Menu.css">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <link rel="stylesheet" href="menu/css/styleIndex.css">

    <!-- Fuente de letras -->

    <link href="https://fonts.googleapis.com/css?family=Oswald|Raleway|Varela+Round" rel="stylesheet">

    <!-- jQuery -->
    <script src="menu/js/jquery.js"></script>
    <script src="menu/js/jquery-ui.js"></script>
    <script>
        function openNav() {
            document.getElementById("mySidenav").style.width = "210px";
        }

        /* Set the width of the side navigation to 0 */
        function closeNav() {
            document.getElementById("mySidenav").style.width = "0";
        }

    </script>
        <script type="text/javascript">
        
            window.$zopim || (function (d, s) {
                var z = $zopim = function (c) {
                    z._.push(c)
                }, $ = z.s =
                        d.createElement(s), e = d.getElementsByTagName(s)[0];
                z.set = function (o) {
                    z.set.
                            _.push(o)
                };
                z._ = [];
                z.set._ = [];
                $.async = !0;
                $.setAttribute("charset", "utf-8");
                $.src = "//v2.zopim.com/?3ihiLjWyaiZHOzH5z1eFEtaAop6zlLTy";
                z.t = +new Date;
                $.
                        type = "text/javascript";
                e.parentNode.insertBefore($, e)
            })(document, "script");
        </script>
</head>
<!-- Google Analytics -->
<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-71967023-1', 'auto');
    ga('send', 'pageview');

    var url = "https://laravelapi-achargoy.c9users.io";
    
function sendMailtres() {
    event.preventDefault();
    
    if (!$("#nombre").val()|| !$("#apellido").val() || !$("#telefono").val()||
        !$("#correo").val() || !$("#dia").val() || !$("#mes").val()  ||
        !$("#year").val() || !$("#txt_est_1").val() ) {
        //notificaction("Completa todos los campos requeridos", "warning");
       
        console.log("Completa todos los campos requeridos");
        alert("Completa todo los campos");
    } else {
     
        $.ajax({
            url: url + "/contactar/cliente",
            type: "POST",
            data: {
                nombre: $("#nombre").val(),
                apellido: $("#apellido").val(),
                celular: $("#telefono").val(),
                email: $("#correo").val(),
                dia: $("#dia").val(),
                mes: $("#mes").val(),
                year: $("#year").val(),
                plaza: $("#txt_est_1").val(),
            },
            dataType: "JSON",
            beforeSend: function () {
                $("#wait").show();
            },
            success: function (respuesta) {
                if(respuesta.status == 0 ){
                    alert(respuesta.mensaje);
                } else{
                    alert("Muchas gracias tu mensaje ha sido enviado exitosamente");
                    document.getElementById("contactForm").reset();
                }
            },
            error: function (respuesta) { 
                alert(respuesta.responseText.mensaje);
            },
            complete: function () {
                $("#wait").hide();
            }
        });
    }
}
function sendMailtresMovil() {
    event.preventDefault();
    
    if (!$("#nombreM").val()|| !$("#apellidoM").val() || !$("#telefonoM").val()||
        !$("#correoM").val() || !$("#diaM").val() || !$("#mesM").val()  ||
        !$("#yearM").val() || !$("#txt_est_1M").val() ) {
        //notificaction("Completa todos los campos requeridos", "warning");
       
        console.log("Completa todos los campos requeridos");
        alert("Completa todo los campos");
    } else {
     
        $.ajax({
            url: url + "/contactar/cliente",
            type: "POST",
            data: {
                nombre: $("#nombreM").val(),
                apellido: $("#apellidoM").val(),
                celular: $("#telefonoM").val(),
                email: $("#correoM").val(),
                dia: $("#diaM").val(),
                mes: $("#mesM").val(),
                year: $("#yearM").val(),
                plaza: $("#txt_est_1M").val(),
            },
            dataType: "JSON",
            beforeSend: function () {
                $("#wait").show();
            },
            success: function (respuesta) {
                if(respuesta.status == 0 ){
                    alert(respuesta.mensaje);
                } else{
                    alert("Muchas gracias tu mensaje ha sido enviado exitosamente");
                    document.getElementById("contactFormM").reset();
                }
            },
            error: function (respuesta) { 
                alert(respuesta.responseText.mensaje);
            },
            complete: function () {
                $("#wait").hide();
            }
        });
    }
}
</script>

<!--Incluyendo menú -->
<?php include('menu.php'); ?>


<!-- Buscador -->
<div>
    <div>
        <img id = "imageHome" src="images/IMG-HOME-WEB-JUN2017-AZUL.jpg" class="img-fluid" alt="Responsive image">
        <div class="telefono" style="margin-top: 8%">
            <span style="font-family: 'Varela Round', sans-serif;">Teléfono 01 800 200 0440</span>
        </div>
        <br>
        <div class="positionTitle">
            <span class="titulo">
                <h1 style="font-family: 'Varela Round', sans-serif;">REVIMEX </h1><h3 style="font-family: 'Varela Round', sans-serif;">tu mejor opción</h3><br>
            </span>
            <input id="search" class="buscador" type="text" placeholder=" Introduce tu ciudad de interés" name="search"> 
        </div>
    </div>
</div>

<div class="Publicity" id="lasCercanas">PROPIEDADES DESTACADAS</div>

<!-- Propiedades -->

<?php include('propiedadesDes.php'); ?> 

<!-- Promociones -->

<?php include('promocionesInd.php'); ?>

<script>
    
    $.ajax({
             url: "https://laravelapi-achargoy.c9users.io/propiedades/estadis",
             type: "POST",
             data: {

             },
             dataType: "JSON",

             success: function (respuesta) {

                //console.log(respuesta);

                var estaAhorro = respuesta["Estadistica"].Ahorrado;

                var estaVendido = respuesta["Estadistica"].Vendidas;

                var estaTenemos = respuesta["Estadistica"].Tenemos;

                $('.estaAhorro').text(estaAhorro);   

                $('.estaVendidas').text(estaVendido);

                $('.estaDisponibles').text(estaTenemos);

             },
             error: function (respuesta) {
                 console.log(respuesta);
             },


         /*}).done(function(pcoloniaAjax){
            $('#pcolonia').text(pcoloniaAjax);
         */});

</script>

<!-- Datos estadístico -->

<div id="estaGran">
<div class="espacioEsta"></div>
<center><div class="estiloEstad">
    
    <div class="caja1">
        <img class="fondos" src="images/AHORRADO-IMG.jpg">
        <div class="cotenedorAzul">
        </div>
        <div><span class="LetrasEstEncabe">HEMOS AHORRADO</span><br><span class="LetrasEstCifra"><span class="estaAhorro"></span></div>
        <div class="cotenedorBlanco">
        </div>
        <div class="LetrasEstMedio">MILLONES A LAS FAMILIAS</div>
        <div class="cotenedorAzulClaro">
        </div>
        <div class="iconEsta"><img style="width: 56%; height: auto;" src="images/AHORRADO.png"></div>
        <div class="cotenedorAzulFuerte">
        </div>
    </div>
    
    <div class="caja1">
        <img class="fondos" src="images/VENDIDO-IMG.jpg">
        <div class="cotenedorAzul">
        </div>
        <div><span class="LetrasEstEncabe">HEMOS VENDIDO</span><br><span class="LetrasEstCifra"><span class="estaVendidas"></span></div>
        <div class="cotenedorBlanco">
        </div>
        <div class="LetrasEstMedio">VIVIENDAS REHABILITADAS</div>
        <div class="cotenedorAzulClaro">
        </div>
        <div class="iconEsta"><img style="width: 56%; height: auto;" src="images/VENDIDO.png"></div>
        <div class="cotenedorAzulFuerte">
        </div>
    </div>
    
    <div class="caja1">
        <img class="fondos" src="images/TENEMOS-IMG.jpg">
        <div class="cotenedorAzul">
        </div>
        <div><span class="LetrasEstEncabe">TENEMOS</span><br><span class="LetrasEstCifra"><span class="estaDisponibles"></span></div>
        <div class="cotenedorBlanco">
        </div>
        <div class="LetrasEstMedio">CASAS LISTAS PARA TI</div>
        <div class="cotenedorAzulClaro">
        </div>
        <div class="iconEsta"><img style="width: 56%; height: auto;" src="images/TENEMOS.png"></div>
        <div class="cotenedorAzulFuerte">
        </div>
    </div>
    

</div></center>
</div>

<!-- Datos estadístico móvil -->

<div id="estaChi">
<div class="espacioEsta"></div>
<center>
    
    <div class="caja1">
        <img class="fondosMovil" src="images/AHORRADO-IMG.jpg">
        <div class="cotenedorAzulMovil">
        </div>
        <div><span class="LetrasEstEncabeMovil">HEMOS AHORRADO</span><br><span class="LetrasEstCifraMovil"><span class="estaAhorro"></span></div>
        <div class="cotenedorBlancoMovil">
        </div>
        <div class="LetrasEstMedioMovil">MILLONES A LAS FAMILIAS</div>
        <div class="cotenedorAzulClaroMovil">
        </div>
        <div class="iconEsta"><img class="tamanocerdo" src="images/AHORRADO.png"></div>
        <div class="cotenedorAzulFuerteMovil">
        </div>
    </div>

    <div class="caja1">
        <img class="fondosMovil" src="images/VENDIDO-IMG.jpg">
        <div class="cotenedorAzulMovil">
        </div>
        <div><span class="LetrasEstEncabeMovil">HEMOS VENDIDO</span><br><span class="LetrasEstCifraMovil"><span class="estaVendidas"></span></div>
        <div class="cotenedorBlancoMovil">
        </div>
        <div class="LetrasEstMedioMovil">VIVIENDAS REHABILITADAS</div>
        <div class="cotenedorAzulClaroMovil">
        </div>
        <div class="iconEsta"><img class="tamanocerdo" src="images/VENDIDO.png"></div>
        <div class="cotenedorAzulFuerteMovil">
        </div>
    </div>
    <div class="caja1">
        <img class="fondosMovil" src="images/TENEMOS-IMG.jpg">
        <div class="cotenedorAzulMovil">
        </div>
        <div><span class="LetrasEstEncabeMovil">TENEMOS</span><br><span class="LetrasEstCifraMovil"><span class="estaDisponibles"></span></div>
        <div class="cotenedorBlancoMovil">
        </div>
        <div class="LetrasEstMedioMovil">CASAS LISTAS PARA TI</div>
        <div class="cotenedorAzulClaroMovil">
        </div>
        <div class="iconEsta"><img class="tamanocerdo" src="images/TENEMOS.png"></div>
        <div class="cotenedorAzulFuerteMovil">
        </div>
    </div>

</center>
</div>

<!-- Noticias y Formulario -->

<?php include('notiRevi.php'); ?>

    <div id="contenido1" class="col-sm-6 " style="align-content: center;">
  
        <div class="Contacta" id="lasCercanas">CONTÁCTANOS</div>
        <div class="espacioForm"></div>

        <form id="miform" class="miform" id="contactForm" action="" method="POST">
            <input id="nombre" type="text" type="text" placeholder="    Nombre:" class="misInput">
            <input id="apellido" type="text" type="text" placeholder="    Apellido:" class="misInput">
            <input id="telefono" type="text"  type="text" placeholder="    Teléfono:" class="misInput">
            <input id="correo" type="text" type="text" placeholder="    E-mail:" class="misInput">

            <table>
              <tr>
                <td style="width: 4%"></td>
                <td style="width: 31%;">Fecha de nacimiento:</td>
                <td type="text" class="campoanio"><input id="dia" class="inputFecha" placeholder="dd"></td>
                <td type="text" class="campoanio"><input id="mes" class="inputFecha" placeholder="mm"></td>
                <td type="text"  class="campoanio"><input id="year" class="inputFecha" placeholder="aaaa"></td>
              </tr>
            </table>
            <select  class="miSelect" id="txt_est_1" style="width: 100%">
                <option value="" class="optioncasa">¿Dónde quieres tu casa?</option>
                <option class="optioncasa" value="Aguascalientes">Aguascalientes</option>
                <option class="optioncasa" value="Baja California">Baja California</option>
                <option class="optioncasa" value="Baja California Sur">Baja California Sur</option>
                <option class="optioncasa" value="Chihuahua">Chihuahua</option>
                <option class="optioncasa" value="Coahuila">Coahuila</option>
                <option class="optioncasa" value="Durango">Durango</option>
                <option class="optioncasa" value="Guanajuato">Guanajuato</option>
                <option class="optioncasa" value="Jalisco">Jalisco</option>
                <option class="optioncasa" value="Michoacan">Michoacan</option>
                <option class="optioncasa" value="Nuevo León">Nuevo León</option>
                <option class="optioncasa" value="Queretaro">Queretaro</option>
                <option class="optioncasa" value="Quintana Roo">Quintana Roo</option>
                <option class="optioncasa" value="San Luis Potosi">San Luis Potosi</option>
                <option class="optioncasa" value="Sinaloa">Sinaloa</option>
                <option class="optioncasa" value="Sonora">Sonora</option>
                <option class="optioncasa" value="Tamaulipas">Tamaulipas</option>
                <option class="optioncasa" value="Veracruz">Veracruz</option>
            </select>
            <table>
                <tr>
                    <td style="padding-top: 7%"></td>
                </tr>
                <tr>
                    <td class="botonFinal"><input class="estiloBorrar" type="button" onclick="reset()" value="Borrar"></td>
                    <td class="botonFinal"><input class="estiloEnviar" type="submit" onclick="sendMailtres()" value="Enviar"></td>
                </tr>
            </table>
        </form>

    </div>

    <!-- Formulario Móvil -->

    <div id="contenido2" class="col-sm-6 " style="align-content: center; margin-top: 7%;">
  
        <div class="Contacta" id="lasCercanas">CONTÁCTANOS</div>
        <div class="espacioForm"></div>

        <form id="contactFormM" class="miform">
            <input id="nombreM" type="text" type="text" placeholder="    Nombre:" class="misInput">
            <input id="apellidoM" type="text" type="text" placeholder="    Apellido:" class="misInput">
            <input id="telefonoM" type="text"  type="text" placeholder="    Teléfono:" class="misInput">
            <input id="correoM" type="text" type="text" placeholder="    E-mail:" class="misInput">

            <table>
              <tr>
                <td style="width: 4%"></td>
                <td style="width: 31%;">Fecha de nacimiento:</td>
                <td type="text" class="campoanio"><input id="diaM" class="inputFecha" placeholder="dd"></td>
                <td type="text" class="campoanio"><input id="mesM" class="inputFecha" placeholder="mm"></td>
                <td type="text"  class="campoanio"><input id="yearM" class="inputFecha" placeholder="aaaa"></td>
              </tr>
            </table>
            <select  class="miSelect" id="txt_est_1M" style="width: 100%">
                <option value="" class="optioncasa">¿Dónde quieres tu casa?</option>
                <option class="optioncasa" value="Aguascalientes">Aguascalientes</option>
                <option class="optioncasa" value="Baja California">Baja California</option>
                <option class="optioncasa" value="Baja California Sur">Baja California Sur</option>
                <option class="optioncasa" value="Chihuahua">Chihuahua</option>
                <option class="optioncasa" value="Coahuila">Coahuila</option>
                <option class="optioncasa" value="Durango">Durango</option>
                <option class="optioncasa" value="Guanajuato">Guanajuato</option>
                <option class="optioncasa" value="Jalisco">Jalisco</option>
                <option class="optioncasa" value="Michoacan">Michoacan</option>
                <option class="optioncasa" value="Nuevo León">Nuevo León</option>
                <option class="optioncasa" value="Queretaro">Queretaro</option>
                <option class="optioncasa" value="Quintana Roo">Quintana Roo</option>
                <option class="optioncasa" value="San Luis Potosi">San Luis Potosi</option>
                <option class="optioncasa" value="Sinaloa">Sinaloa</option>
                <option class="optioncasa" value="Sonora">Sonora</option>
                <option class="optioncasa" value="Tamaulipas">Tamaulipas</option>
                <option class="optioncasa" value="Veracruz">Veracruz</option>
            </select>
            <table>
                <tr>
                    <td style="padding-top: 7%"></td>
                </tr>
                <tr>
                    <td class="botonFinal"><input class="estiloBorrar" type="button" onclick="reset()" value="Borrar"></td>
                    <td class="botonFinal"><input class="estiloEnviar" type="submit" onclick="sendMailtresMovil()" value="Enviar"></td>
                </tr>
            </table>
        </form>

    </div>

  </div>
</div>

<!-- fin Noticias y Formulario -->


<div id="contenido1" class="col-sm-6 ">
    <center><input class="estiloBorrar" type="button" onclick="window.location.href='http://revimex.mx/sitio/noticias.html'" value="Más noticias" style="margin-top: -72px; padding: 4px 21px; font-size: 16px"></center>
</div>


<div id="btns">
    <div id = "preca" >
        <a href="http://precalificaciones.infonavit.org.mx/Precalificacion/precalif.xhtml?tipoProducto=CI" target="_blank">
            <img border="0" alt="" src="images/Preca.png">
        </a>
    </div>
    <div id = "NSS" >
        <a href="http://portal.infonavit.org.mx/wps/wcm/connect/Infonavit/Trabajadores/Obten+tu+Numero+de+Seguridad+Social+(NSS)/" target="_blank">
            <img border="0" alt="" src="images/NSS.png">
        </a>
    </div>
</div>

<!-- Footer -->

<?php include('footerRevi.php'); ?>

<!-- Bootstrap Core JavaScript -->
<script src="menu/js/bootstrap.min.js"></script>

<script src="menu/js/buscador.js"></script> 

</body>
</html>